<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTweet;
use App\Tweet;
use Illuminate\Support\Facades\Auth;

class TweetController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()){
            return redirect('login');
        }

        $tweets = Tweet::getAllTweets();
        return view('home', compact('tweets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTweet $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTweet $request)
    {
        if(!Auth::check()){
            return redirect('login');
        }

        $validated = $request->rules();

        $tweet = new Tweet();
        $tweet->user_id = Auth::id();
        $tweet->tweet = $request->get('tweet');
        $tweet->save();
        return redirect('home');
    }
}
