<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tweet extends Model
{

    public static function getAllTweets(){

        return DB::table('tweets')->join('users', 'users.id', '=', 'tweets.user_id')
                ->select('users.name', 'tweets.tweet', 'tweets.created_at')
            ->orderBy('created_at', 'desc')->get();
    }

    public function post()
    {
        return $this->belongsTo('App\User');
    }

}
