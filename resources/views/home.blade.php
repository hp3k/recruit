@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form method="POST" action="{{ route('storeTweet') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="tweet" class="col-sm-4 col-form-label text-md-right">Tweet</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" autofocus="autofocus" required="required" name="tweet"></textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add new tweet
                                    </button>
                                </div>
                            </div>
                        </form>
                        <br />
                    @foreach($tweets as $tweet)
                        <div class="alert alert-info">
                            <p class="alert alert-info" role="alert"> {{ $tweet->tweet }} {{ $tweet->name }} {{ $tweet->created_at }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
