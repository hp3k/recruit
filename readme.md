# Instalation

Check the file .env and change those lines:

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=homestead

In console put:

php artisan migrate

# Start application

In console put:

php artisan serve

